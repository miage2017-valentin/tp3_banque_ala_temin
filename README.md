# Installation
Commencez tout d'abord par cloner le projet à 
l'aide d'un terminal ou d'un IDE:
```
git clone https://bitbucket.org/miage2017-valentin/tp3_banque_ala_temin.git
```

####Configuration
Ouvrez tout d'abord le projet avec Netbeans (le projet a été développé sous Netbeans 8.2).
Une erreur peut apparaître, elle mentionne que des références vers des librairies sont erronées.
Il suffit alors de cliquer sur "Resolve" et de sélectionner les deux .jar 
qui se trouvent à la racine du projet (itext-2.1.7.jar et jbcrypt-0.4).

Si aucune erreur n'apparaît, il suffit d'ajouter les libraries suivantes
à la partie EJB du projet: itext-2.1.7.jar et jbcrypt-0.4 ainsi que PrimeFaces 5.0 à la partie WAR du projet.

Vous pouvez alors lancer le projet.

#Les différents comptes
Administrateur
- Identifiant : admin@mail.com
- Mot de passe: admin

Conseiller
- Identifiant: conseiller@mail.com
- Mot de passe:  conseiller

Conseiller numéro 2
- Identifiant: conseiller2@mail.com
- Mot de passe: conseiller2

Clients
- Identifiant: mail.x@mail.com => x allant de 0 à 29, par exemple : mail.15@mail.com
- Mot de passe: client
