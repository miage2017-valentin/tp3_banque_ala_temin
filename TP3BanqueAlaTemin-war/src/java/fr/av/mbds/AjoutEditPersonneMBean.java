/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.Conseiller;
import fr.av.mbds.entities.Personne;
import fr.av.mbds.enums.ERolePersonne;
import fr.av.mbds.login.LoginMBean;
import fr.av.mbds.sessions.GestionnaireAdministrateur;
import fr.av.mbds.sessions.GestionnaireClient;
import fr.av.mbds.sessions.GestionnairePersonne;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author valentin
 */
@Named(value = "ajoutPersonneMBean")
@ViewScoped
public class AjoutEditPersonneMBean implements Serializable {

    private Long idPersonne;
    private ERolePersonne role;
    private Personne nouvellePersonne;

    @EJB
    private GestionnaireClient gestionnaireClient;
    @EJB
    private GestionnaireAdministrateur gestionnaireAdministrateur;
    @EJB
    private GestionnairePersonne gestionnairePersonne;

    private Personne personneActuelle;

    public AjoutEditPersonneMBean() {
        nouvellePersonne = new Personne();
        this.personneActuelle = LoginMBean.getCurrentPersonne();
        if (this.personneActuelle.hasRole(ERolePersonne.CONSEILLER.toString())) {
            this.role = ERolePersonne.CLIENT;
        }
    }

    public Personne getNouvellePersonne() {
        return nouvellePersonne;
    }

    public void setNouvellePersonne(Personne nouvellePersonne) {
        this.nouvellePersonne = nouvellePersonne;
    }

    public String creerPersonne() {
        Personne pers = null;

        try {
            if (!LoginMBean.getCurrentPersonne().hasRole(ERolePersonne.CLIENT.toString())) {
                switch (this.role) {
                    case ADMINISTRATEUR:
                        pers = this.nouvellePersonne.toAdministrateur();
                        break;
                    case CLIENT:
                        pers = this.nouvellePersonne.toClient();
                        if (this.personneActuelle.hasRole(ERolePersonne.CONSEILLER.toString())) {
                            ((Client) pers).setSonConseiller((Conseiller) this.personneActuelle);
                        }
                        break;
                    case CONSEILLER:
                        pers = this.nouvellePersonne.toConseiller();
                        break;
                    default:
                        throw new Exception("Type de la personne inconnu");
                }
                gestionnairePersonne.creerPersonne(pers);
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Personne créée avec succès", null));
            }else{
                throw new Exception("Vous n'êtes pas authorisé à effectuer cette action.");
            }
        } catch (NullPointerException ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Type de la personne non spécifiée", null));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
        return "";
    }

    public ERolePersonne getRole() {
        return role;
    }

    public void setRole(ERolePersonne role) {
        this.role = role;
    }

    public Long getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(Long idPersonne) {
        this.idPersonne = idPersonne;
    }

    public void loadPersonne() throws Exception {
        if (idPersonne != null) {
            this.nouvellePersonne = gestionnairePersonne.getPersonne(idPersonne);
            this.role = ERolePersonne.getRoleByName(this.nouvellePersonne.getClass().getSimpleName());
        }
    }

    public String modifierPersonne() {
        try {
            gestionnairePersonne.modifierPersonne(this.nouvellePersonne);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Personne modifiée avec succès", null));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return "";
        }
        return "";
    }
}
