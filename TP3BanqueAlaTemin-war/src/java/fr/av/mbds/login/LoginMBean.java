package fr.av.mbds.login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import fr.av.mbds.entities.Personne;
import fr.av.mbds.sessions.GestionnairePersonne;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author valentin
 */
@Named(value = "loginMBean")
@SessionScoped
public class LoginMBean implements Serializable {

    @EJB
    GestionnairePersonne gestionnairePersonne;

    private String name;
    private String password;

    /**
     * Creates a new instance of LoginMBean
     */
    public LoginMBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String seConnecter() {
        try {
            Personne personne = gestionnairePersonne.seConnecter(name, password);
            if (personne.hasRole("administrateur")) {
                return "restrict/admin/CompteBancaireList.xhtml?faces-redirect=true";
            } else if (personne.hasRole("client")) {
                return "restrict/client/mesComptes.xhtml?faces-redirect=true";
            } else if (personne.hasRole("conseiller")) {
                return "restrict/conseiller/mesClients.xhtml?faces-redirect=true";
            } else {
                return "restrict/accesRestreint?faces-redirect=true";
            }
        }catch(Exception ex){
            FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return "";
        }
    }

    public String seDeconnecter() {
        gestionnairePersonne.seDeconnecter();
        return "/accesPublic.xhtml?faces-redirect=true";
    }

    public static Personne getCurrentPersonne() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);

        return (Personne) session.getAttribute("utilisateur");
    }
}
