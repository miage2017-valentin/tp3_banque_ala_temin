/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.av.mbds.primefaces;
import fr.av.mbds.entities.CompteBancaire;
import java.lang.reflect.Field;
import java.util.Comparator;
import org.primefaces.model.SortOrder;
/**
 *
 * @author Alison Temin
 */
public class LazySorter implements Comparator<CompteBancaire>{
     private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(CompteBancaire compte1, CompteBancaire compte2) {
        try {
            Field field = CompteBancaire.class.getDeclaredField(this.sortField);
            field.setAccessible(true);
            Object value1 = field.get(compte1);
            Object value2 = field.get(compte2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
    
}
