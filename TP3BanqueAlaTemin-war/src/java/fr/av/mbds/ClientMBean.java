/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.entities.Conseiller;
import fr.av.mbds.entities.Personne;
import fr.av.mbds.enums.ERolePersonne;
import fr.av.mbds.login.LoginMBean;
import fr.av.mbds.sessions.GestionnaireClient;
import fr.av.mbds.sessions.GestionnaireConseiller;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author valentin
 */
@Named(value = "clientMBean")
@ViewScoped
public class ClientMBean implements Serializable {
    private Client cibleAssignConseiller;
    private LazyDataModel<Client> lazyModel;
    @EJB
    private GestionnaireClient gestionnaireClient;
    @EJB
    private GestionnaireConseiller gestionnaireConseiller;
    private Client selectedClient;
    private Conseiller selectedConseiller;
    private final Personne personneActuelle;
    private List<Conseiller> listConseiller;
    private CompteBancaire selectedCompteBancaire;
    
    private Converter conseillerConverter = new Converter() {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            Conseiller cons = gestionnaireConseiller.getConseiller(Long.parseLong(value));
            return cons;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            Conseiller cons = (Conseiller) value;
            return cons.getId().toString();
        }
    };

    /**
     * Creates a new instance of ClientMBean
     */
    public ClientMBean() {
        this.personneActuelle = LoginMBean.getCurrentPersonne();
        this.lazyModel = new LazyDataModel<Client>() {
            List<Client> dataSource;

            @Override
            public Client getRowData(String rowKey) {
                for (Client client : dataSource) {
                    if (client.getId() == Long.parseLong(rowKey)) {
                        return client;
                    }
                }

                return null;
            }

            @Override
            public Object getRowKey(Client client) {
                return client.getId();
            }

            public List load(int start, int nb, String nomColonne,
                    SortOrder ordreTri, Map map) {
                List<Client> listClient = null;
                if(personneActuelle.hasRole(ERolePersonne.ADMINISTRATEUR.toString())){
                    listClient = gestionnaireClient.getClients(start, nb);
                    if (!map.isEmpty()) {
                        listClient = gestionnaireClient.getClients();
                    }
                }else if(personneActuelle.hasRole(ERolePersonne.CONSEILLER.toString())){
                   listClient = gestionnaireClient.getClientsByConseiller((Conseiller) personneActuelle, start, nb);
                    if (!map.isEmpty()) {
                        listClient = gestionnaireClient.getClientsByConseiller((Conseiller) personneActuelle);
                    } 
                }
                dataSource = listClient;
                List<Client> listReturn = new ArrayList<>();
                System.out.println("Tri à partir de " + start
                        + " jusqu'à " + nb + " élèments");

                int count = 0;
                int index = 0;
                //filter
                for (Client client : listClient) {
                    boolean match = true;

                    if (map != null) {
                        for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
                            try {
                                String filterProperty = it.next();
                                Object filterValue = map.get(filterProperty);
                                Field field;
                                String fieldValue;
                                if (filterProperty.startsWith("sonConseiller.")) {
                                    filterProperty = filterProperty.substring(filterProperty.indexOf(".") + 1);
                                    field = Conseiller.class.getSuperclass().getDeclaredField(filterProperty);
                                    field.setAccessible(true);
                                    fieldValue = String.valueOf(field.get(client.getSonConseiller()));
                                } else {
                                    field = Conseiller.class.getSuperclass().getDeclaredField(filterProperty);
                                    field.setAccessible(true);
                                    fieldValue = String.valueOf(field.get(client));
                                }

                                if (filterValue == null || fieldValue.contains(filterValue.toString())) {
                                    match = true;
                                } else {
                                    match = false;
                                    break;
                                }
                            } catch (Exception e) {
                                match = false;
                            }
                        }
                    }
                    if (match) {
                        //Avec filtre
                        if (!map.isEmpty()) {
                            count++;
                            if (index >= start && index < start + nb) {
                                listReturn.add(client);
                            }
                            index++;
                        } else {
                            //Sans filtre
                            listReturn.add(client);
                        }
                    }
                }

                int nbRow = 0;
                if (!map.isEmpty()) {
                    nbRow = count;
                } else {
                    if(personneActuelle.hasRole(ERolePersonne.ADMINISTRATEUR.toString())){
                        nbRow = gestionnaireClient.count();
                    }else if(personneActuelle.hasRole(ERolePersonne.CONSEILLER.toString())){
                        nbRow = gestionnaireClient.countByConseiller((Conseiller) personneActuelle);
                    }
                }

                this.setRowCount(nbRow);

                return listReturn;

            }
        };
    }

    public LazyDataModel<Client> getLazyModel() {
        return lazyModel;
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
        this.selectedClient = selectedClient;
    }

    //PrimeFaces
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Client selectionné", ((Client) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public Client getCibleAssignConseiller() {
        return cibleAssignConseiller;
    }

    public void setCibleAssignConseiller(Client cibleAssignConseiller) {
        this.cibleAssignConseiller = cibleAssignConseiller;
        this.selectedConseiller = cibleAssignConseiller.getSonConseiller();
    }
    
    public Converter getConseillerConverter() {
        return conseillerConverter;
    }

    public void setConseillerConverter(Converter conseillerConverter) {
        this.conseillerConverter = conseillerConverter;
    }
    
    public void assignerConseiller(){
        this.cibleAssignConseiller.setSonConseiller(selectedConseiller);
        gestionnaireClient.majClient(this.cibleAssignConseiller);
    }

    public List<Conseiller> getListConseiller() {
        return listConseiller;
    }

    public void setListConseiller(List<Conseiller> listConseiller) {
        this.listConseiller = listConseiller;
    }
    
    @PostConstruct
    public void postConstruct(){
        this.listConseiller = gestionnaireConseiller.getAllConseiller();
    }

    public Conseiller getSelectedConseiller() {
        return selectedConseiller;
    }

    public void setSelectedConseiller(Conseiller selectedConseiller) {
        this.selectedConseiller = selectedConseiller;
    }

    public CompteBancaire getSelectedCompteBancaire() {
        return selectedCompteBancaire;
    }

    public void setSelectedCompteBancaire(CompteBancaire selectedCompteBancaire) {
        this.selectedCompteBancaire = selectedCompteBancaire;
    }
}
