/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.login.LoginMBean;
import fr.av.mbds.sessions.GestionnaireDeCompteBancaire;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author valentin
 */
@Named(value = "transfertCompteBancaireMBean")
@ViewScoped
public class TransfertCompteBancaireMBean implements Serializable {
    private List<CompteBancaire> destListCompteBancaire;  
    private List<CompteBancaire> srcListCompteBancaire;  
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    private CompteBancaire srcCompteBancaire;
    private CompteBancaire destCompteBancaire;
    private int montant;
    
    private Converter compteBancaireConverter = new Converter() {  
  
        @Override  
        public Object getAsObject(FacesContext context, UIComponent component, String value) {  
            CompteBancaire cb = gestionnaireDeCompteBancaire.getCompte(Long.parseLong(value));
            return cb;  
       }  
        @Override  
        public String getAsString(FacesContext context, UIComponent component, Object value) {  
            CompteBancaire mm = (CompteBancaire) value;  
            return mm.getId().toString();
        }  
    };

    public Converter getCompteBancaireConverter() {
        return compteBancaireConverter;
    }

    public CompteBancaire getSrcCompteBancaire() {
        return srcCompteBancaire;
    }

    public void setSrcCompteBancaire(CompteBancaire srcCompteBancaire) {
        this.srcCompteBancaire = srcCompteBancaire;
    }
    
    public CompteBancaire getDestCompteBancaire() {
        return destCompteBancaire;
    }

    public void setDestCompteBancaire(CompteBancaire destCompteBancaire) {
        this.destCompteBancaire = destCompteBancaire;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }
    
    public TransfertCompteBancaireMBean() {
    }
    
    @PostConstruct
    private void postConstruct () {
        this.destListCompteBancaire = gestionnaireDeCompteBancaire.getAllComptes();
        if(LoginMBean.getCurrentPersonne().hasRole("administrateur")){
            this.srcListCompteBancaire = gestionnaireDeCompteBancaire.getAllComptes();
        }else if(LoginMBean.getCurrentPersonne().hasRole("client")){
            this.srcListCompteBancaire = gestionnaireDeCompteBancaire.getComptesByPersonne(LoginMBean.getCurrentPersonne());
        }
    }
    
    public String transfert() throws Exception{
        System.out.println("Transfert");
        try{
            gestionnaireDeCompteBancaire.transfert(srcCompteBancaire, destCompteBancaire, montant);
            if(LoginMBean.getCurrentPersonne().hasRole("administrateur")){
                return "/restrict/admin/CompteBancaireList";
            }else if(LoginMBean.getCurrentPersonne().hasRole("client")){
                return "/restrict/client/mesComptes.xhtml";
            }else{
                return "TransfertCompteBancaire";
            }
        }catch(Exception ex){
            FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return "TransfertCompteBancaire";
        }
    }

    public List<CompteBancaire> getDestListCompteBancaire() {
        return destListCompteBancaire;
    }

    public void setDestListCompteBancaire(List<CompteBancaire> destListCompteBancaire) {
        this.destListCompteBancaire = destListCompteBancaire;
    }

    public List<CompteBancaire> getSrcListCompteBancaire() {
        return srcListCompteBancaire;
    }

    public void setSrcListCompteBancaire(List<CompteBancaire> srcListCompteBancaire) {
        this.srcListCompteBancaire = srcListCompteBancaire;
    }
    
}
