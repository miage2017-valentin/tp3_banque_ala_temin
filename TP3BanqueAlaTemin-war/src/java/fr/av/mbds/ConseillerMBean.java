/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Conseiller;
import fr.av.mbds.sessions.GestionnaireConseiller;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author valentin
 */
@Named(value = "conseillerMBean")
@ViewScoped
public class ConseillerMBean implements Serializable {

    private LazyDataModel<Conseiller> lazyModel;
    @EJB
    private GestionnaireConseiller gestionnaireConseiller;

    private Conseiller selectedConseiller;

    public ConseillerMBean() {
        this.lazyModel = new LazyDataModel<Conseiller>() {
            List<Conseiller> dataSource;

            @Override
            public Conseiller getRowData(String rowKey) {
                for (Conseiller conseiller : dataSource) {
                    if (conseiller.getId() == Long.parseLong(rowKey)) {
                        return conseiller;
                    }
                }

                return null;
            }

            @Override
            public Object getRowKey(Conseiller conseiller) {
                return conseiller.getId();
            }

            public List load(int start, int nb, String nomColonne,
                    SortOrder ordreTri, Map map) {
                List<Conseiller> listConseiller = gestionnaireConseiller.getConseillers(start, nb);
                if (!map.isEmpty()) {
                    listConseiller = gestionnaireConseiller.getAllConseiller();
                }
                dataSource = listConseiller;
                List<Conseiller> listReturn = new ArrayList<>();
                System.out.println("Tri à partir de " + start
                        + " jusqu'à " + nb + " élèments");

                int count = 0;
                int index = 0;
                //filter
                for (Conseiller conseiller : listConseiller) {
                    boolean match = true;

                    if (map != null) {
                        for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
                            try {
                                String filterProperty = it.next();
                                Object filterValue = map.get(filterProperty);
                                Field field;
                                String fieldValue;

                                field = Conseiller.class.getSuperclass().getDeclaredField(filterProperty);
                                field.setAccessible(true);
                                fieldValue = String.valueOf(field.get(conseiller));

                                if (filterValue == null || fieldValue.contains(filterValue.toString())) {
                                    match = true;
                                } else {
                                    match = false;
                                    break;
                                }
                            } catch (Exception e) {
                                match = false;
                            }
                        }
                    }
                    if (match) {
                        //Avec filtre
                        if (!map.isEmpty()) {
                            count++;
                            if (index >= start && index < start + nb) {
                                listReturn.add(conseiller);
                            }
                            index++;
                        } else {
                            //Sans filtre
                            listReturn.add(conseiller);
                        }
                    }
                }

                int nbRow = 0;
                if (!map.isEmpty()) {
                    nbRow = count;
                } else {
                    nbRow = gestionnaireConseiller.count();
                }

                this.setRowCount(nbRow);

                return listReturn;

            }
        };
    }

    public LazyDataModel<Conseiller> getLazyModel() {
        return lazyModel;
    }

    public Conseiller getSelectedConseiller() {
        return selectedConseiller;
    }

    public void setSelectedConseiller(Conseiller selectedConseiller) {
        this.selectedConseiller = selectedConseiller;
    }

    //PrimeFaces
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Conseiller selectionné", ((Conseiller) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String supprimerConseiller(Conseiller conseiller){
        gestionnaireConseiller.supprimerConseiller(conseiller);
        return "";
    }
}
