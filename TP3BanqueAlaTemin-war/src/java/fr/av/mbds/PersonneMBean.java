/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Personne;
import fr.av.mbds.sessions.GestionnairePersonne;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author valentin
 */
@Named(value = "personneMBean")
@ViewScoped
public class PersonneMBean implements Serializable{

    @EJB
    private GestionnairePersonne gestionnairePersonne;
    
    public PersonneMBean() {
    }
    
    public String supprimerPersonne(Personne p){
        gestionnairePersonne.supprimerPersonne(p);
        return "";
    }
    
    public String showEditPersonne(Long id){
        return "/restrict/commun/AjoutEditPersonne.xhtml?idPersonne=" + id+"&faces-redirect=true";
    }
}
