/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.sessions.GestionnaireClient;
import fr.av.mbds.sessions.GestionnaireDeCompteBancaire;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Alison Temin
 */
@Named(value = "ajoutCompteBancaireMBean")
@ViewScoped
public class AjoutCompteBancaireMBean implements Serializable {

    @EJB
    private GestionnaireClient gestionnaireClient;
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    private CompteBancaire compteBancaire = new CompteBancaire();

    private Converter clientConverter = new Converter() {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            Client cb = gestionnaireClient.getClient(Long.parseLong(value));
            return cb;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            Client mm = (Client) value;
            return mm.getId().toString();
        }
    };
    
    @EJB
    private GestionnaireClient clientManager;

    public Converter getClientConverter() {
        return clientConverter;
    }

    public List<Client> getAllClients() {
        return clientManager.getClients();
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public String create() {
        gestionnaireDeCompteBancaire.creerCompte(compteBancaire);
        FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Compte créé avec succès !", null));
        return "";
    }

    /**
     * Creates a new instance of AjoutCompteBancaireMBean
     */

    public AjoutCompteBancaireMBean() {
    }

}
