/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.enums.ERolePersonne;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author valentin
 */
@Named(value = "constantMBean")
@ApplicationScoped
public class ConstantMBean {
    
    public ERolePersonne[] getRoleValues(){
        return ERolePersonne.values();
    }
}
