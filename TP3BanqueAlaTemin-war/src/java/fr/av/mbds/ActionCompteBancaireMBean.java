/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.enums.ERolePersonne;
import fr.av.mbds.login.LoginMBean;
import fr.av.mbds.sessions.GestionnaireDeCompteBancaire;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author valentin
 */
@Named(value = "actionCompteBancaireMBean")
@ViewScoped
public class ActionCompteBancaireMBean implements Serializable {

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;

    private Long idCompteBancaire;
    private CompteBancaire compteBancaire;
    private int nbAjout;
    private int nbRetirer;

    public int getNbAjout() {
        return nbAjout;
    }

    public void setNbAjout(int nbAjout) {
        this.nbAjout = nbAjout;
    }

    public int getNbRetirer() {
        return nbRetirer;
    }

    public void setNbRetirer(int nbRetirer) {
        this.nbRetirer = nbRetirer;
    }

    public Long getIdCompteBancaire() {
        return idCompteBancaire;
    }

    public void setIdCompteBancaire(Long idCompteBancaire) {
        this.idCompteBancaire = idCompteBancaire;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public void loadCompteBancaire() {
        this.compteBancaire = gestionnaireDeCompteBancaire.getCompte(this.idCompteBancaire);
    }

    /**
     * Action handler - met à jour la base de données en fonction du client
     * passé en paramètres, et renvoie vers la page qui affiche la liste des
     * clients.
     */
    public void update() {
        try {
            if (!LoginMBean.getCurrentPersonne().hasRole(ERolePersonne.CLIENT.toString())) {
                gestionnaireDeCompteBancaire.deposer(compteBancaire, nbAjout);
                gestionnaireDeCompteBancaire.retirer(compteBancaire, nbRetirer);
                this.compteBancaire = gestionnaireDeCompteBancaire.update(compteBancaire);
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Compte bancaire mis à jour avec succès", null));
            } else {
                throw new Exception("Vous n'êtes pas authorisé à effectuer cette action.");
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /**
     * Creates a new instance of ActionCompteBancaire
     */
    public ActionCompteBancaireMBean() {
    }

}
