/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Administrateur;
import fr.av.mbds.sessions.GestionnaireAdministrateur;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author valentin
 */
@Named(value = "administrateurMBean")
@ViewScoped
public class AdministrateurMBean implements Serializable {

    private LazyDataModel<Administrateur> lazyModel;
    @EJB
    private GestionnaireAdministrateur gestionnaireAdministrateur;

    private Administrateur selectedAdministrateur;

    /**
     * Creates a new instance of ClientMBean
     */
    public AdministrateurMBean() {
        this.lazyModel = new LazyDataModel<Administrateur>() {
            List<Administrateur> dataSource;

            @Override
            public Administrateur getRowData(String rowKey) {
                for (Administrateur admin : dataSource) {
                    if (admin.getId() == Long.parseLong(rowKey)) {
                        return admin;
                    }
                }

                return null;
            }

            @Override
            public Object getRowKey(Administrateur admin) {
                return admin.getId();
            }

            public List load(int start, int nb, String nomColonne,
                    SortOrder ordreTri, Map map) {
                List<Administrateur> listAdmin = gestionnaireAdministrateur.getAdministrateurs(start, nb);
                if (!map.isEmpty()) {
                    listAdmin = gestionnaireAdministrateur.getAdministrateurs();
                }
                dataSource = listAdmin;
                List<Administrateur> listReturn = new ArrayList<>();
                System.out.println("Tri à partir de " + start
                        + " jusqu'à " + nb + " élèments");

                int count = 0;
                int index = 0;
                //filter
                for (Administrateur admin : listAdmin) {
                    boolean match = true;

                    if (map != null) {
                        for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
                            try {
                                String filterProperty = it.next();
                                Object filterValue = map.get(filterProperty);
                                Field field;
                                String fieldValue;
                                
                                field = Administrateur.class.getSuperclass().getDeclaredField(filterProperty);
                                field.setAccessible(true);
                                fieldValue = String.valueOf(field.get(admin));

                                if (filterValue == null || fieldValue.contains(filterValue.toString())) {
                                    match = true;
                                } else {
                                    match = false;
                                    break;
                                }
                            } catch (Exception e) {
                                match = false;
                            }
                        }
                    }
                    if (match) {
                        //Avec filtre
                        if (!map.isEmpty()) {
                            count++;
                            if (index >= start && index < start + nb) {
                                listReturn.add(admin);
                            }
                            index++;
                        } else {
                            //Sans filtre
                            listReturn.add(admin);
                        }
                    }
                }

                int nbRow = 0;
                if (!map.isEmpty()) {
                    nbRow = count;
                } else {
                    nbRow = gestionnaireAdministrateur.count();
                }

                this.setRowCount(nbRow);

                return listReturn;

            }
        };
    }

    public LazyDataModel<Administrateur> getLazyModel() {
        return lazyModel;
    }

    public Administrateur getSelectedAdministrateur() {
        return selectedAdministrateur;
    }

    public void setSelectedAdministrateur(Administrateur selectedAdmin) {
        this.selectedAdministrateur = selectedAdmin;
    }

    //PrimeFaces
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Administrateur selectionné", ((Administrateur) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
