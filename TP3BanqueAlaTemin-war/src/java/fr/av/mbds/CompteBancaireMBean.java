/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.entities.Personne;
import fr.av.mbds.login.LoginMBean;
import fr.av.mbds.primefaces.LazySorter;
import fr.av.mbds.sessions.GestionnaireDeCompteBancaire;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author valentin
 */
@Named(value = "compteBancaireMBean")
@ViewScoped
public class CompteBancaireMBean implements Serializable {

    private List<CompteBancaire> listCompteBancaire;
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;

    private LazyDataModel<CompteBancaire> lazyModel;

    private CompteBancaire selectedCompteBancaire;

    private Personne personneActuelle;

    public LazyDataModel<CompteBancaire> getLazyModel() {
        return lazyModel;
    }

    /**
     * Creates a new instance of CompteBancaireMBean
     */
    public CompteBancaireMBean() {
        this.lazyModel = new LazyDataModel<CompteBancaire>() {
            List<CompteBancaire> dataSource;

            @Override
            public CompteBancaire getRowData(String rowKey) {
                for (CompteBancaire cb : dataSource) {
                    if (cb.getId() == Long.parseLong(rowKey)) {
                        return cb;
                    }
                }

                return null;
            }

            @Override
            public Object getRowKey(CompteBancaire cb) {
                return cb.getId();
            }

            public List load(int start, int nb, String nomColonne,
                    SortOrder ordreTri, Map map) {
                List<CompteBancaire> listCompteBancaire;
                if (personneActuelle.hasRole("administrateur")) {
                    if(!map.isEmpty()){
                        listCompteBancaire = gestionnaireDeCompteBancaire.getAllComptes();
                    }else{
                        listCompteBancaire = gestionnaireDeCompteBancaire.getComptes(start, nb);
                    }
                } else {
                    listCompteBancaire = gestionnaireDeCompteBancaire.getComptesByPersonne(personneActuelle, start, nb);
                }
                dataSource = listCompteBancaire;
                List<CompteBancaire> listReturn = new ArrayList<>();
                System.out.println("Tri à partir de " + start
                        + " jusqu'à " + nb + " élèments");

                //filter
                for (CompteBancaire compteBancaire : listCompteBancaire) {
                    boolean match = true;

                    if (map != null) {
                        for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
                            try {
                                String filterProperty = it.next();
                                Object filterValue = map.get(filterProperty);
                                Field field;
                                String fieldValue;
                                if (filterProperty.startsWith("proprietaire.")) {
                                    filterProperty = filterProperty.substring(filterProperty.indexOf(".") + 1);
                                    field = Client.class.getSuperclass().getDeclaredField(filterProperty);
                                    field.setAccessible(true);
                                    fieldValue = String.valueOf(field.get(compteBancaire.getProprietaire()));
                                } else {
                                    field = CompteBancaire.class.getDeclaredField(filterProperty);
                                    field.setAccessible(true);
                                    fieldValue = String.valueOf(field.get(compteBancaire));
                                }

                                if (filterValue == null || fieldValue.contains(filterValue.toString())) {
                                    match = true;
                                } else {
                                    match = false;
                                    break;
                                }
                            } catch (Exception e) {
                                match = false;
                            }
                        }
                    }
                    if (match) {
                        listReturn.add(compteBancaire);
                    }
                }
                //sort
                if (nomColonne != null) {
                    Collections.sort(listReturn, new LazySorter(nomColonne, ordreTri));
                }

                int nbRow = 0;
                if (personneActuelle.hasRole("administrateur")) {
                    if (!map.isEmpty()) {
                        nbRow = listReturn.size();
                    } else {
                        nbRow = gestionnaireDeCompteBancaire.count();
                    }
                } else {
                    nbRow = gestionnaireDeCompteBancaire.countByPersonne(personneActuelle);
                }

                this.setRowCount(nbRow);
                return listReturn;
            }
        };
    }

    @PostConstruct
    private void postConstruct() {
        //this.listCompteBancaire = gestionnaireDeCompteBancaire.getAllComptes();
        this.personneActuelle = LoginMBean.getCurrentPersonne();
    }

    public List<CompteBancaire> getListCompteBancaire() {
        return listCompteBancaire;
    }

    public String showActionCompteBancaire(int compteBancaireId) {
        return "/restrict/commun/ActionCompteBancaire?idCompteBancaire=" + compteBancaireId;
    }

    public void deleteCompteBancaire(CompteBancaire cb) {
        gestionnaireDeCompteBancaire.deleteCompteBancaire(cb);
    }

    //PrimeFaces
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Compte bancaire sélectionné", ((CompteBancaire) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public CompteBancaire getSelectedCompteBancaire() {
        return selectedCompteBancaire;
    }

    public void setSelectedCompteBancaire(CompteBancaire selectedCompteBancaire) {
        this.selectedCompteBancaire = selectedCompteBancaire;
    }
    
    public void supprimerCoProprietaire(Client coProprio, CompteBancaire cb){
        cb.getCoProprietaires().remove(coProprio);
        this.gestionnaireDeCompteBancaire.majCompteBancaire(cb);
    }
}
