/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.security;

import fr.av.mbds.entities.Administrateur;
import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.Conseiller;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Restriction implements Filter {
    public static final String ACCES_PUBLIC     = "/faces/accesPublic.xhtml";
    public static final String ERROR_403  = "/faces/restrict/error_403.xhtml";
    public static final String ATT_SESSION_USER = "utilisateur";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String currentUrl = getCurrentUrlFromRequest(req);
        
        if (session == null ||
            session.getAttribute(ATT_SESSION_USER) == null) { // change "user" for the session attribute you have defined
            
            response.sendRedirect(request.getContextPath() + ACCES_PUBLIC); // No logged-in user found, so redirect to login page.
        }else if(currentUrl.contains("/admin/") &&  session.getAttribute(ATT_SESSION_USER).getClass() != Administrateur.class){
            response.sendRedirect(request.getContextPath() + ERROR_403);
        }else if(currentUrl.contains("/client/") && session.getAttribute(ATT_SESSION_USER).getClass() != Client.class){
            response.sendRedirect(request.getContextPath() + ERROR_403);
        }else if(currentUrl.contains("/conseiller/") && session.getAttribute(ATT_SESSION_USER).getClass() != Conseiller.class){
            response.sendRedirect(request.getContextPath() + ERROR_403);
        }else{
            chain.doFilter(req, res);
        }
    }
    

    @Override
    public void destroy() {
    }
    
    private static String getCurrentUrlFromRequest(ServletRequest request)
    {
       if (! (request instanceof HttpServletRequest))
           return null;

       return getCurrentUrlFromRequest((HttpServletRequest)request);
    }
    
    private static String getCurrentUrlFromRequest(HttpServletRequest request)
    {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null)
            return requestURL.toString();

        return requestURL.append('?').append(queryString).toString();
    }
}
