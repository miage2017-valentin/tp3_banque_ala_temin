/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.enums;

/**
 *
 * @author valentin
 */
public enum ERolePersonne {
    ADMINISTRATEUR, CONSEILLER,CLIENT;
    
    public static ERolePersonne getRoleByName(String name) throws Exception{
        for(ERolePersonne role: ERolePersonne.values()){
            if(role.toString().equalsIgnoreCase(name)){
                return role;
            }
        }
        throw new Exception("Erreur: Rôle introuvable");
    }
}
