/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.entities;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author valentin
 */
@Entity
public class CarteIdentite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String identifiantNational;
    private LocalDate dateNaissance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CarteIdentite() {
    }

    public CarteIdentite(String identifiantNational, LocalDate dateNaissance) {
        this.identifiantNational = identifiantNational;
        this.dateNaissance = dateNaissance;
    }

    public String getIdentifiantNational() {
        return identifiantNational;
    }

    public void setIdentifiantNational(String identifiantNational) {
        this.identifiantNational = identifiantNational;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarteIdentite)) {
            return false;
        }
        CarteIdentite other = (CarteIdentite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.av.mbds.entities.CarteIdentitee[ id=" + id + " ]";
    }
    
}
