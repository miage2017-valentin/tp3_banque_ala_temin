/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author valentin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.count", query = "select count(c) from Client c"),
    @NamedQuery(name = "Client.countByConseiller", query = "select count(c) from Client c where c.sonConseiller=:conseiller"),
    @NamedQuery(name = "Client.countByPersonne", query = "select count(c) from Client c where c.sonConseiller=:conseiller"),
    @NamedQuery(name = "Client.findByConseiller", query = "SELECT c FROM Client c where c.sonConseiller=:conseiller"),
    @NamedQuery(name = "Client.detachConseiller", query = "UPDATE Client cl SET cl.sonConseiller = null where cl.sonConseiller=:conseiller")
})
public class Client extends Personne implements Serializable  {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    private Conseiller sonConseiller;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="proprietaire", cascade = CascadeType.REMOVE)
    private List<CompteBancaire> sesComptesBancaires = new ArrayList<>();
    
    @ManyToMany(mappedBy="coProprietaires")
    private List<CompteBancaire> comptesAnnexes = new ArrayList<>();
    
    @OneToOne(cascade=CascadeType.ALL)
    private CarteIdentite saCarte;
    
    
    public Client() {
        super();
    }
    
    public Client(String adresseMail, String motDePasse, String nom, String prenom, String adresse, String ville) {
        super(adresseMail, motDePasse, nom, prenom, adresse, ville);
    }

    public CarteIdentite getSaCarte() {
        return saCarte;
    }

    public void setSaCarte(CarteIdentite saCarte) {
        this.saCarte = saCarte;
    }

    public Conseiller getSonConseiller() {
        return sonConseiller;
    }

    public void setSonConseiller(Conseiller sonConseiller) {
        this.sonConseiller = sonConseiller;
    }

    public List<CompteBancaire> getSesComptesBancaires() {
        return sesComptesBancaires;
    }

    public List<CompteBancaire> getComptesAnnexes() {
        return comptesAnnexes;
    }

    public void setComptesAnnexes(List<CompteBancaire> comptesAnnexes) {
        this.comptesAnnexes = comptesAnnexes;
    }
}
