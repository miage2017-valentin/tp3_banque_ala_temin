package fr.av.mbds.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author valentin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Conseiller.findAll", query = "SELECT c FROM Conseiller c"),
    @NamedQuery(name="Conseiller.count", query="SELECT count(c) from Conseiller c")
})
public class Conseiller  extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    public Conseiller() {
        super();
    }

    public Conseiller(String adresseMail, String motDePasse, String nom, String prenom, String adresse, String ville) {
        super(adresseMail, motDePasse, nom, prenom, adresse, ville);
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="sonConseiller")
    private List<Client> sesClients = new ArrayList<>();

    public List<Client> getSesClients() {
        return sesClients;
    }
    
}
