/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author valentin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Personne.findByName", query = "SELECT p FROM Personne p where p.nom=:name"),
    @NamedQuery(name = "Personne.findByMail", query = "SELECT p FROM Personne p where p.adresseMail=:mail"),
    @NamedQuery(name = "Personne.findByIdAndMail", query = "SELECT p FROM Personne p where p.adresseMail=:mail and p.id!=:id")
})
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(
    name = "role"
)
public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(unique=true)
    private String adresseMail;
    private String nom;
    private String prenom;
    private String adresse;
    private String ville;
    private String motDePasse;
    
    public Personne() {
    }
    
    public Personne(String adresseMail, String motDePasse, String nom, String prenom, String adresse, String ville) {
        this.adresseMail = adresseMail;
        this.motDePasse = BCrypt.hashpw(motDePasse, BCrypt.gensalt());
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.ville = ville;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.av.mbds.entities.Personne[ id=" + id + " ]";
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
    
    public String getNomComplet(){
        return this.nom + " " + this.prenom;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
    
    public boolean hasRole(String roleName){
        return roleName.equalsIgnoreCase(this.getClass().getSimpleName());
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }
    
    public Client toClient(){
        Client client = new Client(this.adresseMail,this.motDePasse,this.nom,this.prenom,this.adresse,this.ville);
        return client;
    }
    
    public Administrateur toAdministrateur(){
        Administrateur admin = new Administrateur(this.adresseMail,this.motDePasse,this.nom,this.prenom,this.adresse,this.ville);
        return admin;
    }
    
    public Conseiller toConseiller(){
        Conseiller conseiller = new Conseiller(this.adresseMail,this.motDePasse,this.nom,this.prenom,this.adresse,this.ville);
        return conseiller;
    }
}
