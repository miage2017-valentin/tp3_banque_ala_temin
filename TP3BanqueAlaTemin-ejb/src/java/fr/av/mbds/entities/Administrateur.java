/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.entities;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author valentin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Administrateur.findAll", query = "SELECT a FROM Administrateur a")
    ,
    @NamedQuery(name = "Administrateur.count", query = "select count(a) from Administrateur a")
})
@DiscriminatorValue(value="Administrateur")
public class Administrateur extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public Administrateur(){
        super();
    }
    public Administrateur(String adresseMail, String motDePasse, String nom, String prenom, String adresse, String ville) {
        super(adresseMail, motDePasse, nom, prenom, adresse, ville);
    }
}
