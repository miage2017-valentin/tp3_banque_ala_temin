/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author valentin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "CompteBancaire.findAll", query = "SELECT c FROM CompteBancaire c")
    ,
    @NamedQuery(name = "CompteBancaire.count", query = "select count(c) from CompteBancaire c"),
    @NamedQuery(name = "CompteBancaire.countByPersonne", query = "select count(c) from CompteBancaire c where c.proprietaire=:proprietaire"),
    @NamedQuery(name = "CompteBancaire.findByPersonne", query = "SELECT c FROM CompteBancaire c where c.proprietaire=:proprietaire")
})
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private int solde;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REFRESH)
    @JoinColumn(nullable=false)
    private Client proprietaire;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="compteBancaire", cascade= CascadeType.REMOVE)
    private List<OperationBancaire> sesOperations = new ArrayList<>();
    
    @ManyToMany
    private List<Client> coProprietaires;

    
    public CompteBancaire() {
    }

    public CompteBancaire(int solde) {
        this.solde = solde;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Client proprietaire) {
        this.proprietaire = proprietaire;
    }

    public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteBancaire)) {
            return false;
        }
        CompteBancaire other = (CompteBancaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.av.mbds.CompteBancaire[ id=" + id + " ]";
    }

    public void addOperation(OperationBancaire op){
        this.sesOperations.add(op);
    }

    public List<OperationBancaire> getSesOperations() {
        return sesOperations;
    }

    public List<Client> getCoProprietaires() {
        return coProprietaires;
    }

    public void setCoProprietaires(List<Client> coProprietaires) {
        this.coProprietaires = coProprietaires;
    }
}
