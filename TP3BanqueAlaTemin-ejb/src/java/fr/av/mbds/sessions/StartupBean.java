/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

/**
 *
 * @author valentin
 */
import fr.av.mbds.entities.Administrateur;
import fr.av.mbds.entities.Conseiller;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.ejb.Singleton;

@Singleton
@Startup
public class StartupBean {

    @EJB
    private GestionnaireConseiller gestionnaireConseiller;

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    
    @EJB
    private GestionnaireAdministrateur gestionnaireAdministrateur;

    @PostConstruct
    public void init() {
        Administrateur admin = new Administrateur("admin@mail.com","admin","admin","admin","blablabla avenue", "Nice");
        Conseiller conseiller = new Conseiller("conseiller@mail.com","conseiller","conseiller","robert", "1234 avenue des poules", "Nice");
        Conseiller conseiller2 = new Conseiller("conseiller2@mail.com","conseiller2","conseiller2","roberta", "1233 avenue des poules", "Nice");
        
        gestionnaireAdministrateur.creerAdministrateur(admin);
        gestionnaireConseiller.creerConseiller(conseiller);
        gestionnaireConseiller.creerConseiller(conseiller2);
        gestionnaireDeCompteBancaire.creerComptesTest(30, 2000000);
    }

    @PreDestroy
    public void destroy() {
      /* Shutdown stuff here */
    }

}
