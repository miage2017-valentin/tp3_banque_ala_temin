/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.Conseiller;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnaireClient {

    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerClient(Client c) {
        em.persist(c);
    }
    
    public void majClient(Client c){
        em.merge(c);
    }

    public Client getClient(Long id) {
        return em.find(Client.class, id);
    }

    public List<Client> getClients() {
        Query query = em.createNamedQuery("Client.findAll");
        return query.getResultList();
    }

    public List<Client> getClientsByConseiller(Conseiller conseiller){
        Query query = em.createNamedQuery("Client.findByConseiller");
        query.setParameter("conseiller", conseiller);
        return query.getResultList();
    }
    
    public List<Client> getClientsByConseiller(Conseiller conseiller, int start, int nb){
        Query query = em.createNamedQuery("Client.findByConseiller");
        query.setParameter("conseiller", conseiller);
        query.setFirstResult(start);
        query.setMaxResults(nb);
        return query.getResultList();
    }
    
    public List<Client> getClients(int start, int nb){
        Query q = em.createNamedQuery("Client.findAll");
        q.setFirstResult(start);
        q.setMaxResults(nb);
        return q.getResultList();
    }
    
    public int count() {
        Query q = em.createNamedQuery("Client.count");
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }
    
    public int countByConseiller(Conseiller conseiller) {
        Query q = em.createNamedQuery("Client.countByConseiller");
        q.setParameter("conseiller", conseiller);
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }
}
