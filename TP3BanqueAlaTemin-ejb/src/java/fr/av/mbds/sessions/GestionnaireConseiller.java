/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.Conseiller;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnaireConseiller {
    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerConseiller(Conseiller c) {
        em.persist(c);
    }
    
    public List<Conseiller> getAllConseiller(){
        Query query = em.createNamedQuery("Conseiller.findAll");
        return query.getResultList();
    }
    public Conseiller getConseiller(Long id){
        return em.find(Conseiller.class, id);
    }
    public List<Conseiller> getConseillers(int start, int nb){
        Query q = em.createNamedQuery("Conseiller.findAll");
        q.setFirstResult(start);
        q.setMaxResults(nb);
        return q.getResultList();
    }
    
    public int count() {
        Query q = em.createNamedQuery("Conseiller.count");
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }
    
    public void supprimerConseiller(Conseiller conseiller){
        if (!em.contains(conseiller)) {
            conseiller = em.merge(conseiller);
        }
        
        Query q = em.createNamedQuery("Client.detachConseiller");
        q.setParameter("conseiller", conseiller);
        q.executeUpdate();
        
        em.remove(conseiller);
    }
}
