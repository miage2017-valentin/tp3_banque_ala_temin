/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.Administrateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnaireAdministrateur {
    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerAdministrateur(Administrateur a) {
        em.persist(a);
    }
    
    public List<Administrateur> getAdministrateurs(int start, int nb){
        Query q = em.createNamedQuery("Administrateur.findAll");
        q.setFirstResult(start);
        q.setMaxResults(nb);
        return q.getResultList();
    }
    
    public int count() {
        Query q = em.createNamedQuery("Administrateur.count");
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }
    
    public List<Administrateur> getAdministrateurs() {
        Query query = em.createNamedQuery("Administrateur.findAll");
        return query.getResultList();
    }
}
