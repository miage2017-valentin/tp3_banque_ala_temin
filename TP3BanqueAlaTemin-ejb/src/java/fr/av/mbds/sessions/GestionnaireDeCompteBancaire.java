/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.CarteIdentite;
import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.entities.Conseiller;
import fr.av.mbds.entities.ETypeOperation;
import fr.av.mbds.entities.OperationBancaire;
import fr.av.mbds.entities.Personne;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnaireDeCompteBancaire {
    
    @EJB
    private GestionnaireClient gestionnaireClient;
    
    @EJB
    private GestionnaireConseiller gestionnaireConseiller;

    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerCompte(CompteBancaire c) {
        em.persist(c);
        OperationBancaire op = new OperationBancaire(ETypeOperation.CREATION_DU_COMPTE, c.getSolde());
        op.setCompteBancaire(c);
        em.persist(op);
        c.addOperation(op);
        em.merge(c);
        Client cl = c.getProprietaire();
        cl.getSesComptesBancaires().add(c);
        em.merge(cl);
    }

    public List<CompteBancaire> getAllComptes() {
        Query query = em.createNamedQuery("CompteBancaire.findAll");
        return query.getResultList();
    }

    public void creerComptesTest(int nb, int maxSolde) {
        List<Conseiller> listConseiller = gestionnaireConseiller.getAllConseiller();
        ArrayList<Client> coProprietaires = new ArrayList();
        for (int i = 0; i < nb; i++) {
            CompteBancaire cb = new CompteBancaire((int) (Math.random() * (maxSolde)));
            Conseiller c = listConseiller.get((int) (Math.random() * listConseiller.size()));
            CarteIdentite ci = new CarteIdentite(UUID.randomUUID().toString(),LocalDate.now());
            
            //Création du client
            Client client = new Client("mail."+i+"@mail.com","client","Nom_" + i, "Prenom_" + i, "Boulevard des stars", "Paris");
            client.setSonConseiller(c);
            client.setSaCarte(ci);
            c.getSesClients().add(client);
            gestionnaireClient.creerClient(client);
            
            //Attribution du compte bancaire au client & création du compte bancaire
            cb.setProprietaire(client);
            cb.setCoProprietaires(coProprietaires);
            creerCompte(cb);
            for(Client coProprio : coProprietaires){
                coProprio.getComptesAnnexes().add(cb);
                em.merge(coProprio);
            }
            coProprietaires.add(client);
        }
    }

    public CompteBancaire getCompte(Long id) {
        return em.find(CompteBancaire.class, id);
    }

    public CompteBancaire update(CompteBancaire cb) {
        return em.merge(cb);
    }

    public void deleteCompteBancaire(CompteBancaire cb) {
        Client cl = cb.getProprietaire();
        cl.getSesComptesBancaires().remove(cb);
        em.merge(cl);
        if (!em.contains(cb)) {
            cb = em.merge(cb);
        }
        em.remove(cb);
    }

    public void transfert(CompteBancaire cbFrom, CompteBancaire cbTo, int montant) throws Exception {
        if (cbFrom.getSolde() < montant) {
            throw new Exception("Solde insuffisant");
        }

        this.retirer(cbFrom, montant);
        if (Objects.equals(cbFrom.getId(), cbTo.getId())) {
            cbTo = cbFrom;
        }
        this.deposer(cbTo, montant);
        this.update(cbFrom);
        this.update(cbTo);
    }

    public List<CompteBancaire> getComptes(int start, int nombreDeComptes) {
        Query q = em.createNamedQuery("CompteBancaire.findAll");
        q.setFirstResult(start);
        q.setMaxResults(nombreDeComptes);
        return q.getResultList();
    }
    
    public List<CompteBancaire> getComptesByPersonne(Personne personne) {
        Query q = em.createNamedQuery("CompteBancaire.findByPersonne");
        q.setParameter("proprietaire", personne);
        return q.getResultList();
    }
    
    public List<CompteBancaire> getComptesByPersonne(Personne personne, int start, int nombreDeComptes) {
        Query q = em.createNamedQuery("CompteBancaire.findByPersonne");
        q.setParameter("proprietaire", personne);
        q.setFirstResult(start);
        q.setMaxResults(nombreDeComptes);
        return q.getResultList();
    }

    public int count() {
        Query q = em.createNamedQuery("CompteBancaire.count");
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }
    
    public int countByPersonne(Personne personne) {
        Query q = em.createNamedQuery("CompteBancaire.countByPersonne");
        q.setParameter("proprietaire", personne);
        Long c = (Long) q.getSingleResult();
        return c.intValue();
    }

    public CompteBancaire deposer(CompteBancaire cb, int montant) {
        cb.setSolde(cb.getSolde() + montant);
        if(montant > 0){
            cb.addOperation(new OperationBancaire(ETypeOperation.CREDIT, montant));
        }
        return cb;
    }

    public CompteBancaire retirer(CompteBancaire cb, int montant) {
        if (montant < cb.getSolde()) {
            cb.setSolde(cb.getSolde() - montant);
        } else {
            cb.setSolde(0);
        }
        if(montant > 0){
            cb.addOperation(new OperationBancaire(ETypeOperation.DEBIT, montant));
        }
        return cb;
    }
    
    public void majCompteBancaire(CompteBancaire cb){
        em.merge(cb);
    }
}
