/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.Client;
import fr.av.mbds.entities.CompteBancaire;
import fr.av.mbds.entities.Personne;
import fr.av.mbds.security.Restriction;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnairePersonne {

    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerPersonne(Personne p) throws Exception {
        if (this.getPersonByMail(p.getAdresseMail()) == null) {
            em.persist(p);
        } else {
            throw new Exception("Adresse mail déjà utilisé");
        }
    }
    
    public void modifierPersonne(Personne p) throws Exception {
        if (this.getPersonByIdAndMail(p.getId(), p.getAdresseMail()) == null) {
            em.merge(p);
        } else {
            throw new Exception("Adresse mail déjà utilisé");
        }
    }
    
    public void supprimerPersonne(Personne p){
        if(p.getClass() == Client.class){
            Client cl = ((Client)p);
            cl.getComptesAnnexes().clear();
            for(CompteBancaire cb : cl.getSesComptesBancaires()){
                cb.getCoProprietaires().clear();
                em.merge(cb);
            }
            
            p = em.merge(p);
        }
        if (!em.contains(p)) {
            p = em.merge(p);
        }
        em.remove(p);
    }

    public Personne getPersonByMail(String mail) {
        try {
            Query query = em.createNamedQuery("Personne.findByMail");
            query.setParameter("mail", mail);
            return (Personne) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    public Personne getPersonByIdAndMail(Long id, String mail) {
        try {
            Query query = em.createNamedQuery("Personne.findByIdAndMail");
            query.setParameter("mail", mail);
            query.setParameter("id", id);
            return (Personne) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Personne seConnecter(String name, String password) throws Exception {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        Personne pers = this.getPersonByMail(name);
        if (pers != null) {
            if (BCrypt.checkpw(password, pers.getMotDePasse())) {
                session.setAttribute(Restriction.ATT_SESSION_USER, pers);
                return pers;
            } else {
                throw new Exception("Mot de passe incorrect");
            }
        }
        throw new Exception("Utilisateur invalide");
    }

    public boolean seDeconnecter() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute(Restriction.ATT_SESSION_USER, null);
        return true;
    }
    
    public Personne getPersonne(long id){
        return em.find(Personne.class, id);
    }
}
