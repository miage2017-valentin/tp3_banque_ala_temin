/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.av.mbds.sessions;

import fr.av.mbds.entities.OperationBancaire;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author valentin
 */
@Stateless
@LocalBean
public class GestionnaireOperationBancaire {

    @PersistenceContext(unitName = "TP3BanqueAlaTemin-ejbPU")
    private EntityManager em;

    public void creerOperation(OperationBancaire op) {
        em.persist(op);
    }
}
